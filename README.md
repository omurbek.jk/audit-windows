Audit Windows Endpoint 
============================
This stack will perform windows hardening and cis

## Description
This composite will audit linux hosts by running inspec profiles against Windows hardening.

* ![DevSec Windows Baseline](https://github.com/dev-sec/windows-baseline/archive/master.zip "Inspec profile github link")

* ![DevSec Windows Patch Baseline](https://github.com/dev-sec/windows-patch-baseline "Inspec profile github link")

## Hierarchy



## Required variables with no default

**None**


## Required variables with default

**None**


## Optional variables with default

### `AUDIT_ENDPOINT_WINDOWS_PROFILES_ALERT_LIST`:
  * description: 
  * default: windows-baseline, windows-patch-baseline


## Optional variables with no default

### `FILTERED_OBJECTS`:
  * description: JSON object of string or regex of aws objects to include or exclude and tag in audit

## Tags
1. Audit
1. Best Practices
1. CIS

## Categories


## Diagram


## Icon


