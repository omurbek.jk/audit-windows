coreo_agent_selector_rule 'check-windows' do
    action :define
    timeout 120
    control 'check-windows' do
        describe os.windows? do
            it { should eq true }
        end
    end
end

coreo_agent_audit_profile 'windows-baseline' do
    action :define
    selectors ['check-windows']
    profile 'https://github.com/dev-sec/windows-baseline/archive/master.zip'
    timeout 120
end
  
coreo_agent_rule_runner 'audit-endpoint-windows-profiles' do
    action :run
    profiles ${AUDIT_ENDPOINT_WINDOWS_PROFILES_ALERT_LIST}
    filter(${FILTERED_OBJECTS}) if ${FILTERED_OBJECTS}
end
