This composite will audit linux hosts by running inspec profiles against Windows hardening.

* ![DevSec Windows Baseline](https://github.com/dev-sec/windows-baseline/archive/master.zip "Inspec profile github link")

* ![DevSec Windows Patch Baseline](https://github.com/dev-sec/windows-patch-baseline "Inspec profile github link")